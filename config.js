const dotenv = require('dotenv')
dotenv.config()

module.exports = {
    HOST : process.env.MYSQL_HOST,
    USERNAME : process.env.MYSQL_USERNAME,
    PASSWORD : process.env.MYSQL_PASSWORD,
    DATABASE_NAME : process.env.MYSQL_DATABASE_NAME,
    PORT : process.env.PORT || 5000
}
