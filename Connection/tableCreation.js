const pool = require("./connection")
const uuid4 = require('uuid').v4


const contactCreation = `CREATE TABLE IF NOT EXISTS contacts(
    id VARCHAR(100) NOT NULL PRIMARY KEY, 
    name TEXT NOT NULL, 
    ph BIGINT NOT NULL,
    email VARCHAR(200) NOT NULL,
    address TEXT NOT NULL,
    img TEXT NOT NULL);`

const contactValues = `INSERT INTO contacts VALUES
    ('${uuid4()}','Anandhan',1234567892,'xyz@gmail.com','Bangalore','person.jpg'),
    ('${uuid4()}','Naga Anand',1234567891,'naagaanand@gmail.com','Bangalore','person.jpg'),
    ('${uuid4()}','Subram',1234567893,'subram@gmail.com','Bangalore','person.jpg');`

function contactsTableCreation(){
    return new Promise((resolve, reject)=>{
        pool.query(contactCreation, function(error,result){
            if(error){
                reject(error)
            }

            pool.query(`SELECT COUNT(*) AS COUNTER FROM contacts`,(error,result)=>{
                if(error){ 
                    reject(error)
                }
                
                if(result[0]['COUNTER']=== 0){
                    pool.query(contactValues, (error)=>{
                        if(error){
                            reject(error)
                        }
                        resolve()
                    })
                }
            })
        })
    })
}

exports.contactsTableCreation = contactsTableCreation