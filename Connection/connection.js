const sql = require("mysql")
const config = require("../config.js")

const pool = sql.createPool({
    host     : config.HOST,
    user     : config.USERNAME,
    password : config.PASSWORD,
    database : config.DATABASE_NAME
})

module.exports = pool
