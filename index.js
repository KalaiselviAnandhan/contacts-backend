const express = require("express")
const app = express()
const { PORT } = require("./config")
const table = require("./Connection/tableCreation")
const cors = require('cors')
const contacts = require('./Routes/contacts')

async function tablesCreation(){
    try{
        await table.contactsTableCreation()
    }
    catch(error){
        console.error(error)
    }
}
tablesCreation()

app.use(cors())
app.use(express.json())

app.use('/contacts',contacts)


app.listen(PORT)