const contact = require("express").Router()
const pool = require('../Connection/connection')
const uuid4 = require('uuid').v4
const {check, body, validationResult} = require('express-validator')

const checkEmpty = (value) => check(value).isLength({min:1}).withMessage(`${value} Required`)

contact.get("/",(req,res) => {
    const query = `SELECT * FROM contacts`
    pool.query(query, (error,result) => {
        if(error){
            res.status(500).send("Internal Server Error")
        }
        res.send(result)
    })
})

contact.get("/:id",(req,res) => {
    const query = `SELECT * FROM contacts WHERE id = '${req.params.id}'`
    pool.query(query, (error, result) => {
        if(error){
            res.status(500).send("Internal Server Error")
        }
        res.send(result)
    })
})

contact.post("/",[
    checkEmpty('name'),
    checkEmpty('ph'),
    body('ph').isLength({min:10}),
    body('email').isEmail(),
    checkEmpty('address'),
    checkEmpty('img')
],(req,res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) { 
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const query = `INSERT INTO contacts VALUES(?,?,?,?,?,?)`
        pool.query(query, [uuid4(), req.body.name, req.body.ph, req.body.email, req.body.address, req.body.img], 
        (error, result)=>{
            if(error){
                res.status(500).send("Internal Server Error")
            }
            res.send(result)
        })
    }
})

contact.put('/:id',[
    checkEmpty('id'),
    checkEmpty('name'),
    checkEmpty('ph'),
    body('ph').isLength({min:10}),
    body('email').isEmail(),
    checkEmpty('address'),
    checkEmpty('img')
],(req,res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) { 
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const query = `UPDATE contacts SET name = ?, ph = ?, email = ?, address = ?, img = ? WHERE id = ?`
        pool.query(query, [req.body.name, req.body.ph, req.body.email, req.body.address, req.body.img, req.params.id], 
        (error, result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            res.send(result)
        })
    }
})

contact.delete('/:id',(req,res) => {
    const query = `DELETE FROM contacts WHERE id = '${req.params.id}'`
    pool.query(query, (error,result) => {
        if(error){
            res.status(500).send("Internal Server Error")
        }
        res.send(result)
    })
})

module.exports = contact